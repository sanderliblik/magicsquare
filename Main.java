package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;    



    public class Main extends Application {    

        @Override
        public void start(Stage primaryStage) {
            
            Calc calc = new Calc();
            Raam raam = new Raam();
            int dim = calc.getDimension();
            //final int dim = 0;
            int[][] magic = calc.createMagicMatrix(dim);
            int[][] random = calc.createRandomMatrix(dim);
            
            
            GridPane mArea = raam.drawArea(magic, dim);
            GridPane rArea = raam.drawArea(random, dim);
            
            TextField dimension = raam.drawTextField();
            Button calculate = raam.drawButton();
            
            
            HBox squareArea = new HBox();
            squareArea.setAlignment(Pos.CENTER);
            squareArea.setSpacing(5.0); 
            squareArea.setPadding(new Insets(5,5,5,5));
            squareArea.getChildren().addAll(mArea, rArea);
            
            HBox otherArea = new HBox();
            otherArea.setAlignment(Pos.BOTTOM_CENTER);
            otherArea.getChildren().addAll(dimension, calculate);
            
            VBox all = new VBox();
            all.getChildren().addAll(squareArea,otherArea);
            
            Scene scene = new Scene(all, 960, 600);   
           // scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setTitle("Magic Square");
            primaryStage.setScene(scene);
            primaryStage.show();
            
        }

        public static void main(String[] args) {    
            launch(args);
        }    
    }