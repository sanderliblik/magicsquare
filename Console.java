package application;

import application.Calc;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;


public class Console extends Calc {
	
	public static void main (String[] args)	{
		
		int decisions = lotteryOrMath();
		
		if (decisions == 1){
			int dim = getDimension();
			createLotteryTickets(dim);
			System.exit(0);
		}
		else if (decisions == 2){
			int dim = getDimension();
	  		BigInteger totalSq = factorial(dim*dim);
	  	
	  		printMatrix(createMagicMatrix(dim));
	  		printSqInfo(dim, totalSq);

	  		int num = getEntryAccuracy();
	  		int ent = getNumberOfEntries();
	  		//boolean toLog = optionLog();
	  	
	  		boolean toOverWrite = optionOverWrite();
		  	
	  		try{
				writeFileHeader(toOverWrite); // Overwrites file, writes column headers
		  	}
		  	catch (IOException e) {
		  		e.printStackTrace();
		  	}
	  		calculate(dim, num, ent);
		}
	} // End of main method
} // End of Tabel.java class

