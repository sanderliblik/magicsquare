package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Calc  {
	
	/**
	 * Creates an odd n x n sized magic matrix by filling it with integers
	 * ranging from 1 to n^2, starting with 1 at the middle of the top row
	 * (at [0][(n-1)/2]), so that the sum of each row, column and diagonal
	 * is the same. This is achieved with 4 different "moves", which are
	 * executed in order of importance:
	 * 
	 * i - row number (from 0 to n-1)
	 * j - column number (from 0 to n-1)
	 * 
	 * The 1. move inserts the next integer to the slot [n-1][j-1]
	 * and is executed only when:
	 * 
	 * 1. the current row is the topmost
	 * 2. the current column is not the leftmost and 
	 * 3. the value at the next slot is 0
	 * 
	 * The 2. move inserts the next integer to the slot [i-1][n-1]
	 * and is executed only when:
	 * 
	 * 1. the current row is not the topmost
	 * 2. the current column is the leftmost
	 * 3. the value at the next slot ([i-1][n-1]) is 0
	 * 
	 * The 3. move inserts the next integer to the slot [i-1][j-1]
	 * and is executed only when:
	 * 
	 * 1. the current row is not the topmost
	 * 2. the current column is not the leftmost
	 * 3. the value at [i-1][j-1] is 0
	 * 
	 * The 4. move inserts the next integer to the slot [i+1][j]
	 * and is executed only when:
	 * 
	 * 1. the current row is not the last (bottom) one
	 * 2. the value at [i+1][j] is 0
	 * 
	 * As it turns out, this approach is not the simplest and can be
	 * further deduced, but in terms of originality, it will maintain
	 * its current form. 
	 * 
	 * @param n - 1-dimensional length of the matrix to be created
	 * @return matrix - 2-dimensional odd magic array (also known as a matrix)
	 */
	public static int[][] createMagicMatrix(int n){	// täidab sisestatud mõõtmega "maagilise" ruutmaatriksi
		
		int matrix[][] = new int[n][n]; // deklareerib ja algväärtustab "n x n"-mõõtmelise massiivi.			
		int x = 1;		
		int i = 0, j = (n-1)/2;		
		matrix[i][j] = x;			// esimene element, väärtusega 1, asub esimese rea keskmises veerus.
				
			while (x < n*n) { // kuni x on ühe võrra väiksem massiivi mõõtme ruutväärtusest
				
				if (i == 0 && j != 0 && matrix[n-1][j-1] == 0) { // 1. liigutus: viimasesse ritta, üks veerg vasakule
					i += n-1;
					j -= 1;
				}
				else if (i != 0 && j == 0 && matrix[i-1][n-1] == 0) { // 2. liigutus: üks rida üles, viimasesse veergu
					i -= 1;
					j += n-1;
				}					
				else if (i != 0 && j != 0 && matrix[i-1][j-1] == 0) { // 3. liigutus: üles-vasakule 1 rea/veeru võrra
					i -= 1;
					j -= 1;
				}
				else if (i != (n-1) &&  matrix[i+1][j] == 0) { // 4. liigutus: alla 1 rea võrra
					i += 1;
				}
				x++; // lahtrisse kirjutatava täisarvu väärtust tõstetakse 1 võrra
				matrix[i][j]=x; // täisarv kirjutatakse lahtrisse
			}
			return matrix; // tagastab kahemõõtmelise "maagilise" paaritu ruutmaatriksi
		}

	 /**
	 * Creates an n x n sized list, fills it with integers
	 * ranging from 1 to n^2, shuffles the list and transposes
	 * it to a 2-dimensional array
	 * 
	 * @param n - 1-dimensional length of the list to be created
	 * @return array - 2-dimensional array of consecutive integers
	 * 				   in random order without duplicates
	 */
	public static int[][] createRandomMatrix(int n){ 
	
		ArrayList<Integer> list = new ArrayList<Integer>(); // creates list
			for (int k=1; k<(n*n+1); k += 1) {
				list.add(new Integer(k)); // fills list with integers from 1 to n^2
			}
		Collections.shuffle(list); // shuffles list
		
		int[][] array = new int[n][n]; // creates 2D array
		int k = 0;
			for (int i=0; i<n; i++) {
				for (int j=0; j<n; j++){
					array[i][j] = list.get(k).intValue(); // fills array with shuffled list integers
					k++;
				}
			}
		return array;
	}
	
	/**
	 * Prints the input array to the console, column-by-column, row-by-row,
	 * starting from the upper left corner ([0][0]). Depending on the array's
	 * size, the console output is formatted so that the numbers do not "touch"
	 * each other:
	 * 
	 * %2d - for single-digit numbers
	 * %3d - for two-digit numbers
	 * %4d - for three-digit numbers
	 * %5d - for four-digit numbers
	 * %6d - for five-digit numbers
	 * etc etc...
	 * 
	 * @param array - the array to be printed
	 */
	public static void printMatrix(int[][] array){ // print
		
		//System.out.println();
		int n = array.length;
		if (n==3)		
			for (int i=0; i < n; i++){ 
				for (int j=0; j < n; j++){	
						System.out.format("%2d", array[i][j]);
				}
				System.out.println();
			}
		else if (n <= 9)		
			for (int i=0; i < n; i++){ 
				for (int j=0; j < n; j++){	
						System.out.format("%3d", array[i][j]);
				}
				System.out.println();
			}
		else if (n <= 31)		
			for (int i=0; i < n; i++){ 
				for (int j=0; j < n; j++){	
						System.out.format("%4d", array[i][j]);
				}
				System.out.println();
			}
		else if (n <= 99)			
			for (int i=0; i < n; i++){ 
				for (int j=0; j < n; j++){	
						System.out.format("%5d", array[i][j]);
				}
				System.out.println();
			} 
		else if (n >= 101)			
			for (int i=0; i < n; i++){ 
				for (int j=0; j < n; j++){	
						System.out.format("%6d", array[i][j]);
				}
				System.out.println();
			} 
		//System.out.println();
	}
	
	/**
	 * Checks the row sums of the array passed to it. Used for checking random arrays.
	 * Returns true, if the sum of the elements of every row is equal to the magic constant
	 * (n^2+1)*n/2.
	 * 
	 * @param array - 2-dimensional array
	 * 
	 * @return true, if every row sum is equal to the magic constant.
	 * 		   false, if any of them failed to meet the requirement above.
	 */
	public static boolean checkRows(int[][] array){
		
		boolean pass = true;
		int rowSum = 0;
		int n = array.length;
		
		for (int i=0; i<n; i++){
			for (int j=0; j<n; j++){
				rowSum += array[i][j];
			}
			//System.out.print(rowSum + " ");	// Useful for making sure the method works
			if (rowSum != (n*n+1)*n/2){ 
				pass = false;
				return pass;
			}	
			rowSum = 0;
		} 
		//System.out.println();
		return pass;
	}
	
	/**
	 * Checks the column sums of the array passed to it. Used for checking random arrays.
	 * Returns true, if the sum of the elements of every column is equal to the magic constant
	 * (n^2+1)*n/2.
	 * 
	 * @param array - 2-dimensional array
	 * 
	 * @return true, if every column sum is equal to the magic constant.
	 * 		   false, if any of them failed to meet the requirement above.
	 */
	public static boolean checkColumns(int[][] array){ 
		
		boolean pass = true;
		int colSum = 0;
		int n = array.length;
		
		for (int i=0; i<n; i++){
			for (int j=0; j<n; j++){
				colSum += array[j][i];
			}
			//System.out.print(colSum + " "); // Useful for checking if the method works
			if (colSum != (n*n+1)*n/2){ 
				pass = false;
				return pass;
			}	
			colSum = 0;
		} 
		//System.out.println();
		return pass;
	}
		
	/**
	 * Checks the diagonal sums of the array passed to it. Used for checking random arrays.
	 * Returns true, if the sum of the elements of each diagonal is equal to the magic constant
	 * (n^2+1)*n/2.
	 * 
	 * @param array - 2-dimensional array
	 * 
	 * @return true, if both diagonal sums are equal to the magic constant.
	 * 		   false, if one or both of them failed to meet the requirement above.
	 */
	public static boolean checkDiagonals(int[][] array){ // check diagonal sums
		
		boolean pass = false;
		int mainDiagSum = 0;
		int secdDiagSum = 0;
		int n = array.length;
	
		for (int i=0; i<n; i++){
			mainDiagSum += array[i][i];
			secdDiagSum += array[i][n-i-1];
		}
	
		if (mainDiagSum == (n*n+1)*n/2 && secdDiagSum == (n*n+1)*n/2) {
			
			//System.out.println(" D1 = " + mainDiagSum);
			//System.out.println(" D2 = " + secdDiagSum);
			
			pass = true;
			return pass;
		}
	return pass;
	}
	
	/**
	 * Converts a type int[][] array to type int[]. Used for the next method, compareVectors.
	 * 
	 * @param array - int[][] type array to be converted
	 * @return vector - int[] type array
	 */
	public static int[] matrixToVector(int[][] array) {
		
		int k=0;
		int n = array.length;
		int[] vector = new int[n*n];
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < n; j++) { 
	        	vector[k] = array[i][j]; 
	            k++;
	        }
	    }
	    return vector;
	}

	/**
	 * Compares two integer arrays. Used for checking total equivalence between two arrays.
	 * When dealing with magic squares, the larger the square, the more possibilities there
	 * are to arrange a magic square. This method was written before the realization that
	 * with odd magic squares larger than 5x5 elements, no one really knows how many possible
	 * arrangements there are.
	 * 
	 * When dealing with a 3x3 square, there are 9! = 362,880 different ways to arrange one;
	 * meaning the average probability of a random 3x3 square being equivalent with the magic
	 * square the method createMagicMatrix generates, is 1/362,880. But the 3x3 magic square
	 * can be rotated and reflected to produce a total of 8 magic squares. 
	 * 
	 * Contrary to the checkRows/-Columns/-Diagonals methods (which, when used simultaneously
	 * on a 3x3 square, find one of those 8 squares with the probability of 8/362,880 = 1/45,360),
	 * this method only returns true, when the values of both vectors are the same, from start
	 * to end.
	 * 
	 * @see calculate
	 * @param array1 - first array; in this program: randomly ordered array of integers
	 * @param array2 - second array; in this program: "magically" ordered array of integers
	 * 
	 * @return true, if the vectors were the same
	 * 		   false, if they were not
	 */
	public static boolean compareVectors(int[] array1, int[] array2){
		
		int n = array1.length;
		
			for (int i=0; i<n; i++){
				if (array1[i] != array2[i]){
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Prints the vector given as a parameter. Exists solely for easier error checking.
	 * 
	 * @param vector - the vector to be printed
	 */
	public static void printVector(int[] vector){
		
		int n = vector.length;
		for (int i=0; i<n; i++){
			System.out.print(vector[i] + " ");
		}
		System.out.println();
		System.out.println();
	}
	
	/**
	 * Keeps asking for the dimension of an array, until an odd number is entered.
	 * Uses the TextIO.java class to bypass the need to handle exceptions manually.
	 * 
	 * @return dim - the dimension (1-dimensional length) of the array
	 */
	public static int getDimension(){
		
		int dim;
		
		do {
		System.out.print("Enter an odd integer (3,5,7...): "); 
		dim = TextIO.getlnInt();
		//System.out.println();
		} while (dim % 2 == 0);
		
		return dim;
	}
	
	/**
	 * Calculates the factorial of the integer passed to it. This method is used
	 * to find the number of possible different integer arrays of a given size.
	 * The return type is BigInteger, because factorials tend to get very large
	 * very quickly.
	 * 
	 * First, the parameter of type int is converted to the type BigInteger (and
	 * initialized). After that, the BigInteger is multiplied by integers smaller
	 * than itself until that integer is equal to 1.
	 * 
	 * @param dim - the integer we want to factorialize
	 * @return fact - the factorial of the integer passed as an argument
	 */
	public static BigInteger factorial(int dim){
		
		BigInteger fact = BigInteger.valueOf(dim);
		        for (int i = dim-1; i>1 ; i--) {
		        	fact = fact.multiply(BigInteger.valueOf(i));
		        }
		        return fact;
	}
	
	/**
	 * Prints information about dim x dim sized squares: first is the magic constant:
	 * the sum to which each row, column and diagonal elements add up to; the number of 
	 * possible different squares (which is (dim^2)!) and the number of different magic
	 * squares, excluding those obtained by rotations and reflections. As it turns out,
	 * the number of distinct magic squares of sizes greater than 5x5 is uncertain.
	 *  
	 * @param dim - still the 1-dimensional length of the square
	 * @param totalSq - number of possible different squares
	 */
	public static void printSqInfo(int dim, BigInteger totalSq){
		
		NumberFormat factformat = new DecimalFormat();
		//factformat = new DecimalFormat("0.#########E0");
	
		System.out.println("\nMagic constant: " + (dim*dim+1)*dim/2);
	  	System.out.println("Number of possible different squares: " + factformat.format(totalSq));
	  	
	  	if (dim == 3)
	  	System.out.println("Number of different magic squares (excluding rotations/reflections): 1\n");
	  	else if (dim == 5)
	  	System.out.println("Number of different magic squares (excluding rotations/reflections): 275,305,224\n");
	  	else
	 	System.out.println("Number of different magic squares (excluding rotations/reflections): unknown\n");
	}
	
	/**
	 * Asks for the number of entries to write to the output file.
	 * 
	 * @return entries - number of entries to write to the .txt-file
	 */
	public static int getNumberOfEntries(){
		
		int entries;
		
		do {
		System.out.print("Enter number of entries: "); 
		entries = TextIO.getlnInt();
		//System.out.println();
		} while (entries < 0);
		System.out.println();
		
		return entries;
	}
	
	/**
	 * Asks for the number of times the random square has to be magic before the next entry. 
	 * The bigger the entered number, the more "precise" the result of each entry will be.
	 * This method exists to provide the option to choose, whether (for example):
	 *  1. to write 10000 entries, where each entry had to find the magic square once 
	 *  2. to write 100 entries, where each entry had to find the magic square 100 times
	 *  3. to write 1 entry, where that entry had to find the magic square 10000 times
	 *  
	 * @return accuracy - the number of times each entry has to generate a magic square
	 */
	public static int getEntryAccuracy(){
		
		int accuracy;
		
		do {
		System.out.print("Enter number of required succesful attempts/entry: "); 
		accuracy = TextIO.getlnInt();
		//System.out.println();
		} while (accuracy <= 0);
		
		return accuracy;
	}
	
	/**
	 * Gives the user the option to write the program's output into a .txt file.
	 * 
	 * @return true - if the user wants to write the program's output into a .txt file
	 * 		   false - if he/she does not
	 */
	public static boolean optionLog(){
		System.out.print("Log output to log.txt? ");
		boolean toLog = TextIO.getlnBoolean();
		if (toLog) {
			return true;
		}
		return false;
	}
	
	/**
	 * Asks the user, whether he/she wants to overwrite any existing data in the log file.
	 * 
	 * @return true - if they want to overwrite
	 * 		   false - if they do not
	 */
	public static boolean optionOverWrite(){
		System.out.print("Overwrite existing file? ");
		boolean toOverWrite = TextIO.getlnBoolean();
		if (toOverWrite) {
			return true;
		}
		return false;
	}
	
	
	public static int lotteryOrMath(){
		
		int option;
		
		do {
		System.out.print("Do you wish to create lottery tickets (1) or learn math (2)? "); 
		option = TextIO.getlnInt();
		} while (option != 1 && option !=2);
		
		if (option == 1) return 1;
		if (option == 2) return 2;
		
		return option;
	}
	
	/**
	 * Allows the user to choose the criteria needed to win the lottery (3 choices).
	 * 
	 * @return 1 - if all rows are equal to the magic constant, the ticket wins 
	 * 		   2 - if all columns are equal to the magic constant, the ticket wins 
	 * 		   3 - if both diagonals are equal to the magic constant, the ticket wins 
	 */
	public static int chooseGame(){
		
		int option;
		do {
		System.out.print("What shall determine a win? (1 - magic rows, 2 - magic columns, 3 - magic diagonals): "); 
		option = TextIO.getlnInt();
		} while (option != 1 && option != 2 && option != 3);
		
		return option;
	}
	
	public static void createLotteryTickets(int dim){
		
		int counter = 1; // set ticket counter to 1
	  	int losingTickets = 0; // initialize number of losing tickets
	  	int winningTickets = 0; // initialize number of winning tickets
	  	
		int[][] random; //initialize random array
		int option = chooseGame();
		int numTickets = getNumberOfTickets();
		
		if (option == 1){ // playing the "magic rows" game
			for (int i=0; i<numTickets; i++){ // create as many tickets as we wanted
					
					random = createRandomMatrix(dim); // create new random int[][] type array
					
					if (!checkRows(random)){ //  row sums ARE NOT magic -> losing ticket
						System.out.println("Ticket no. " + counter + " | LOSS\n");
						printMatrix(random); // print the ticket along with its number to the console
						System.out.println();
						try{
							writeAllTickets(random, counter); // write the ticket and number to file tickets.txt
							writeLossTickets(random, counter); // write the ticket and  number to losses.txt
						}
						catch (IOException e) { // handle IOException
					  		e.printStackTrace();
					  	}
						counter++; // increase ticket number by 1
						losingTickets++; // add 1 to number of losing tickets
					}
					else { // row sums ARE magic -> winning ticket
						System.out.println("Ticket no. " + counter + " | WIN\n");
						printMatrix(random);
						System.out.println();
						try{
							writeAllTickets(random, counter);
							writeWinTickets(random, counter); // write the ticket and number to wins.txt
						}
						catch (IOException e) {
					  		e.printStackTrace();
					  	}
						counter++;
						winningTickets++;
					}	
	  		}
			System.out.println("Number of winning tickets: "+ winningTickets);
			System.out.println("Number of losing tickets: "+ losingTickets);
		}
		else if (option == 2){
			for (int i=0; i<numTickets; i++){
					
					random = createRandomMatrix(dim);
					
					if (!checkColumns(random)){
						System.out.println("Ticket no. " + counter + " | LOSS\n");
						printMatrix(random);
						System.out.println();
						try{
							writeAllTickets(random, counter);
							writeLossTickets(random, counter);
						}
						catch (IOException e) {
					  		e.printStackTrace();
					  	}
						counter++;
						losingTickets++;
					}
					else {
						System.out.println("Ticket no. " + counter + " | WIN\n");
						printMatrix(random);
						System.out.println();
						try{
							writeAllTickets(random, counter);
							writeWinTickets(random, counter);
						}
						catch (IOException e) {
					  		e.printStackTrace();
					  	}
						counter++;
						winningTickets++;
					}	
	  		}
			System.out.println("Number of winning tickets: "+ winningTickets);
			System.out.println("Number of losing tickets: "+ losingTickets);
		}
		else if (option == 3){
			for (int i=0; i<numTickets; i++){
					
					random = createRandomMatrix(dim);
					
					if (!checkDiagonals(random)){
						System.out.println("Ticket no. " + counter + " | LOSS\n");
						printMatrix(random);
						System.out.println();
						try{
							writeAllTickets(random, counter);
							writeLossTickets(random, counter);
						}
						catch (IOException e) {
					  		e.printStackTrace();
					  	}
						counter++;
						losingTickets++;
					}
					else {
						System.out.println("Ticket no. " + counter + " | WIN\n");
						printMatrix(random);
						System.out.println();
						try{
							writeAllTickets(random, counter);
							writeWinTickets(random, counter);
						}
						catch (IOException e) {
					  		e.printStackTrace();
					  	}
						counter++;
						winningTickets++;
					}	
	  		}
			System.out.println("Number of winning tickets: "+ winningTickets);
			System.out.println("Number of losing tickets: "+ losingTickets);
		}		
	}

	
	public static int getNumberOfTickets(){
		
		int number;
		do {
		System.out.print("Enter number of total tickets to generate (win or lose): "); 
		number = TextIO.getlnInt();
		} while (number <= 0);
		
		return number;
	}
	
	/**
	 * 
	 * @param array
	 * @param counter
	 * @throws IOException
	 */
	public static void writeAllTickets(int[][] array, int counter) throws IOException {
		
		FileWriter fw = new FileWriter("tickets.txt", true);
		int dim = array.length;
		
		fw.write("Ticket number " + counter);
		fw.write(String.format("%n"));
		
		for (int i=0;i<dim;i++){
			for (int j=0; j<dim; j++){
				fw.write(array[i][j] + " ");
			}
			fw.write(String.format("%n"));
		}
		fw.write(String.format("%n"));
		fw.close();	
	}
	
	public static void writeWinTickets(int[][] array, int counter) throws IOException {
		
		FileWriter fw = new FileWriter("wins.txt", true);
		int dim = array.length;
		
		fw.write("Ticket number " + counter);
		fw.write(String.format("%n"));
		
		for (int i=0;i<dim;i++){
			for (int j=0; j<dim; j++){
				fw.write(array[i][j] + " ");
			}
			fw.write(String.format("%n"));
		}
		fw.write(String.format("%n"));
		fw.close();	
	}
	
	public static void writeLossTickets(int[][] array, int counter) throws IOException {
		
		FileWriter fw = new FileWriter("losses.txt", true);
		int dim = array.length;
		
		fw.write("Ticket number " + counter);
		fw.write(String.format("%n"));
		
		for (int i=0;i<dim;i++){
			for (int j=0; j<dim; j++){
				fw.write(array[i][j] + " ");
			}
			fw.write(String.format("%n"));
		}
		fw.write(String.format("%n"));
		fw.close();	
	}
	
	/**
	 * Not too appropriately named, this method generates random dim x dim sized squares
	 * until they meet the given if-clause criteria, which currently is compareVectors();
	 * This in turn means that there is virtually no point in running this method with dim
	 * being greater than 3, as it would most probably take more than 2 billion loops to
	 * find one match.
	 * 
	 * Sure, we could increase the iteration count with some other-than-int type value, but
	 * that is not the main reason this method was written down. In fact, the real reason is
	 * to check, that, given a high amount of entries (and/or high entry efficiency), this
	 * method should prove to us that random number generation does work as mathematicians
	 * say it does.
	 * 
	 * Since the probability of generating that one unique 3x3 magic matrix randomly is exactly
	 * 1/362,880, the average number of executed loops has to converge to exactly 362,880. As 
	 * tests have shown, it could be as low as 400 loops, or as high as 800,000; the more times
	 * you run it, the closer it gets.
	 * 
	 * By replacing what's in the if-clause (leaving, for example, only the checkDiagonals()
	 * method), it is possible to test various other convergence points.
	 * 
	 * The method measures the total time it took for all the entries to complete, as well as
	 * the time it took for each entry. Obviously it also measures, how many loops it takes to
	 * get a bingo.
	 * 
	 * 
	 * @param dim - dimension of array
	 * @param num - number of successful generations required to move to next entry
	 * @param ent - number of entries
	 */
	public static void calculate(int dim, int num, int ent){
		
		long nsEntryStart = 0;
		int counter = 1;
	  	int loopCount = 0;
		double loopTotal = 0;
		
		int[][] magicMatrix = createMagicMatrix(dim); // create the magic array of type int[][]
		int[] magicVector = matrixToVector(magicMatrix); // convert it to type int[]
		
		int[][] randomMatrix; // initialize random array of type int[][]
		int[] randomVector; // initialize random array of type int[]
		
		long nsStart = System.nanoTime(); // start the timer for the whole calculation
		
		for (int s=0; s<ent; s++){ // as many entries as we wanted
	 		nsEntryStart = System.nanoTime(); // start the timer for one entry
	  		for (int t=0; t<num; t++){ // as many successes per entry as we wanted
				for (int u=0; u<1999999999; u++){ // for a decent maximum of 2 billion times
					
					randomMatrix = createRandomMatrix(dim); // create random array of type int[][]
					randomVector = matrixToVector(randomMatrix); //convert it to type int[]
					
								/* the arrays did not match :( */
							if (!compareVectors(randomVector, magicVector)/* ||
								!checkRows(randomMatrix) ||
								!checkColumns(randomMatrix) ||
								!checkDiagonals(randomMatrix)*/
								)
							{
								counter++; // failed attempt -> increase counter by 1
							}
							/* the arrays matched! */
							else {
								//printMatrix(randomMatrix); // for testing 
								//System.out.println();
								loopCount += counter; // add to loopCount the amount of loops it took
								counter = 1;		  // to find a match and set counter back to 1
								break;				  // break out of the 2 billion attempts, because we
													  // found a match
							}
				}
	  		} 
	  		
	  		long nsEntryEnd = System.nanoTime(); // Stop the timer for current entry
	  		long nsEntry = nsEntryEnd - nsEntryStart; // Calculate entry's elapsed time in nanoseconds
	  		double msEntry = nsEntry / 1000000.0; // Convert to milliseconds
	  		double avgLoopsEntry = loopCount*1.0/num; // Average number of loops per successful match
	  		
	  		try { /* Write entry number, dimension, success/entry,
	  				 total entry loop count and average loop count per success to file*/
	  			writeData(s+1, dim, num, loopCount, avgLoopsEntry);
	  		}
	  		catch (IOException e) { //handle exception
	  		    e.printStackTrace();
	  		}
	  		/* Print out all this confusing information to the console */
	  		System.out.println();
	  		System.out.println("Entry: " + (s+1)+ "." + " Dimensions: " + dim + "x" + dim + " Successes: " + num);
	  		System.out.println("Total number of executions: " + loopCount); 
	  		System.out.println("Average number of executions/success: " + avgLoopsEntry*1.0); 	
	  		System.out.println("Total time elapsed: " + msEntry + " milliseconds");
	  		
	  		loopTotal += loopCount; // add the entry's loop count to total amount of loops
	  		msEntry = 0; // reset entry duration
	  		loopCount = 0; // reset entry loop count
	  		
	  	} // all entries have been logged

		long nsEnd = System.nanoTime(); // stop the clock
  		long nsTotal = nsEnd - nsStart; // calculate the total calculation time 
  		double sTotal = nsTotal / 1000000000.0; // convert to seconds
  		double avgLoops = loopTotal*1.0/num/ent; // calculate average number of loops/successes/entry
		
		try { /* write total elapsed time and average loop count of all entries to the end of the file */
  			writeEndData(avgLoops, sTotal);
  		}
  		catch (IOException e) {
  		    e.printStackTrace();
  		} 
	} // end of calculate method, phew...
	
	/**
	 * Appends to or overwrites the log file, depending on whether the user chose
	 * to overwrite or not.
	 * 
	 * @see optionOverWrite
	 * @param toOverWrite - true -> file will be overwritten
	 * 						false -> no overwrite, data will be added
	 * @throws IOException
	 */
	public static void writeFileHeader(boolean toOverWrite) throws IOException {
		
		if (!toOverWrite){
			FileWriter fw = new FileWriter("log.txt", true); // Does not overwrite, adds to end of file
			java.util.Date date = new java.util.Date(); 
		    fw.write(date.toString() + "\n\n"); // Writes date and time of the calculation
			fw.write("Entry\tDimension   Successes\t    Total executions    Executions/success\n");
			fw.close();
		}
		else {
			FileWriter fw = new FileWriter("log.txt"); // Overwrites file
			java.util.Date date = new java.util.Date(); 
			fw.write(date.toString() + "\n\n"); 
			fw.write("Entry\tDimension   Successes\t    Total executions    Executions/success\n");
			fw.close(); 
		}
	}
	
	/**
	 * Writes entry data into the text file. Is called every time an entry is finished.
	 * 
	 * @param ent - entry number
	 * @param dim - array dimension
	 * @param num - entry accuracy (number of required successful magic square generations
	 * 				per entry)
	 * @param loopCount - number of loops made during one entry
	 * @param avgLoopsEntry - average number of loops executed to find one magic square
	 * @throws IOException
	 */
	public static void writeData(int ent, int dim, int num, int loopCount, double avgLoopsEntry) throws IOException {
		
		FileWriter fw = new FileWriter("log.txt", true);
		String formatStr = "%-7s %-11s %-15s %-19s %-19.2f %n";
		fw.write(String.format(formatStr, ent + ".", dim + "x" + dim, num, loopCount, avgLoopsEntry));
		fw.close();	
	}
	
	/**
	 * Writes the "end" data about the last log: this includes the average number of
	 * loops executed (which is the total number of loops executed during all entries,
	 * divided by the entry accuracy, divided by the number of entries) and total time
	 * elapsed during calculation.
	 * 
	 * @param avgLoops - average number of loops executed to generate magic square
	 * @param sTotal - total calculation time in seconds
	 * @throws IOException
	 */
	public static void writeEndData(double avgLoops, double sTotal) throws IOException {
		
		FileWriter fw = new FileWriter("log.txt", true);
		fw.write("\nAverage number of cycles to generate magic square:\t" + avgLoops*1.0 + 
				 "\nTotal calculation time in seconds:\t\t\t" + sTotal + "\n--------" +
				"--------------------------------------------------------------------------------\n");
		fw.close();
	}
} // End of Calc.java class