package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class Raam extends Main {
	
	GridPane drawArea(int[][] array, int dim) {
		
		 GridPane area = new GridPane();
         area.setAlignment(Pos.CENTER);
         area.setMinWidth(480);
         
         for(int i = 0; i < dim; i++){
             for(int j = 0; j < dim; j++){
                
                 int mElement = array[i][j];

                 // Create a new TextField in each Iteration
                 TextField tf = new TextField();
                 tf.setPrefHeight(48);
                 tf.setPrefWidth(48);
                 tf.setAlignment(Pos.CENTER);
                 tf.setEditable(false);
                 
                 tf.setText(mElement + "");

                 // Iterate the Index using the loops
                 area.setRowIndex(tf,i);
                 area.setColumnIndex(tf,j);    
                 area.getChildren().add(tf);
             }
         }
         return area;
	} // End of drawArea method

	Button drawButton(){
		Button calculate = new Button("Calculate");
		calculate.setAlignment(Pos.BOTTOM_RIGHT);
		/*calculate.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	
            	dim = Integer.parseInt(dimension.getText());
            	//int[][] magic = calc.createMagicMatrix(dim);
            	//int[][] random = calc.createRandomMatrix(dim);
            	
            	//do {
            		//random = calc.createRandomMatrix(dim);
            	//} while (random != magic);
            	
            	//final GridPane mArea = raam.drawArea(magic, dim);
                //final GridPane rArea = raam.drawArea(random, dim);
            	
                //calculate.setText("Accepted");
            	
            }
        });*/
		
		return calculate;
	}
	
	TextField drawTextField(){
		
		TextField dimension = new TextField("Enter square dimensions");

		dimension.setAlignment(Pos.BOTTOM_LEFT);
		return dimension;
	}
} // End of Raam.java class



